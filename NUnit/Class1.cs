﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnit
{
    public class Class1
    {
        [TestFixture]
        public class Matikka
        {
            [TestCase]
            public void Add()
            {
                Matikka math = new Matikka();
                Assert.AreEqual(25, math.Add(12, 13));
            }
            public int Add(int x, int y)
            {
                return x + y;
            }
        }
    }
}
